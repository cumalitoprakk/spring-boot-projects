package com.cumali.blog.services;

import com.cumali.blog.domain.CustomUserDetails;
import com.cumali.blog.domain.User;
import com.cumali.blog.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import java.util.Objects;

public class CustomUserDetailsService implements UserDetailsService {

    private static final String USERNAME_OR_PASSWORD_INVALID = "Invalid username or password.";

    @Autowired
    private UserRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepository.findByEmail(username);

        if(Objects.isNull(user)){
            throw new UsernameNotFoundException(USERNAME_OR_PASSWORD_INVALID);
        }

        return new CustomUserDetails(user);
    }
}
